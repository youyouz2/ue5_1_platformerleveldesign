# Platformer Level Design Documentation

Developed with Unreal Engine 5.


## Map Design
- Spawn room lead to a common area which then leads to 3 remote islands. Two of which can be accessed via pathway. One is not accessible until Mortar enemy is killed. Each island has a meaning for player to explore. 
- One island has a dungeon-like building and can only be accessed if player is holding a key. Another island has a secret place to get a sword, which can deal more damage and can be useful for killing Mortar enemy.

## Enemy Design
- Note: all enemy attack will apply knockback to player. during the knockback, player will lose input control.
- Pursuer 
    - walks slowly to chase a player when it's facing player within a distance.
- Stand boss/Mortar 
    - not movebale. shoots fireball that cause explosion effect. hard to dodge the fireball.
- Creative boss/Assasin 
    - sprints super fast. after attack player, it sprints away and give player a chance to attack by waiting for 5 sec. if player did attack, it sprints away again. if player did not attack, it initiate a new attack by sprinting towards player.

## Collectibles Design
- Collectibles can only be acquired by breaking a treasure box that is around the map. 
- 1 Coin = 1 Point that will be displayed on UI
- 1 Medkit = 20 health increments and will be displayed on UI
- Key = Can be used to open a door and will be stored in inventory. 
- Sword = Can be attached to player and deal 40 damages to enemy. Will be stored in inventory.

## Main Player Design
- Control
    - WASD=basic movement
    - space=jump
    - left=melee/sword attack
    - mouse scroll=switch inventory item
- Inventory/backpack
    - Player can use mouse scroll to switch to different inventory slots. The selected slots will be highlighted. 
    - When selecting a sword, player can see an actual sword attached to the hand. 
        - Attack with a sword will have a different attack animation and has double amount of the melee damage.
    - When selecting a key, player can approach any closed door and open the door.

### Downloaded Asset Original Link
- https://itch.io/game-assets/free/tag-unreal-engine
- https://devilsworkshop.itch.io/lowpoly-forest-pack
- https://devilsworkshop.itch.io/low-poly-3d-hex-pack-free
- https://fertile-soil-productions.itch.io/modular-village-pack
- https://www.unrealengine.com/marketplace/en-US/product/m5-vfx-vol2-fire-and-flames
- https://styloo.itch.io/dungeon-asset-pack
- https://quaternius.com/packs/ultimateplatformer.html


---
## Note (to myself for development)
- Project settings – Engine Input – Action Mappings
    - InteractAlt – E – another type of interaction
    - Interact – left mouse click – main interact


## Log
- Day 1: Figured out how to add an animation from Mixamo to the existing character from Unreal Learning Asset. Now player can use WASD, space for jump, left mouse click for attact, mouse scroll up or down to switch backpack item. Added HUD including health bar and backpack w/ 4 slots.
- Day 2: Found many assets for map design. Also copied part of learning assets building to serve as the final checkpoint building.
- Day 3+4: Finishes the basic pursuer enemy class (health + combat). Player now can attact and deal damage to enemy.
- Day 5: Started the Mortar enemy class which should stand still and shoot fireball at player. 
- Day 6: Added collision box for Mortar enemy. Added sword animation+attack for player. Added AI for Mortar enemy to auto face player and shoot fireball at player.
- Day 7: player can pick up sword and add to inventory. backpack scrolling can hold/unhold sword.
- Day 8: enemy attack will apply knockback to player.
- Day 9: added and finished assasin enemy. it walks fast to player and attack. then walks to a random location, wait, and then attack again.
